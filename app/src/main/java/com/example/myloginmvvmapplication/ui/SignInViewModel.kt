package com.example.myloginmvvmapplication.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.internal.wait

class SignInViewModel : ViewModel() {
    var data = MutableLiveData<String>()

    var email  = MutableLiveData<String>()
    var pass = MutableLiveData<String>()

    var errorEmail = MutableLiveData<String?>()
    var errorPass = MutableLiveData<String?>()
    fun signIn(){
        val user = User(email.value.toString(),pass.value.toString())
        if (!user.isEmailValid()){
             errorEmail.value = "eneter valid email address"
          }
        else{
            errorEmail.value = null
        }

         viewModelScope.launch(Dispatchers.IO) {
             delay(2000)
             data.postValue("Success")
         }
    }


    fun sum(a:Int,b:Int) =a+b

}