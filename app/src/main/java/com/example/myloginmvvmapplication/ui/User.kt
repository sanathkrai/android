package com.example.myloginmvvmapplication.ui

import android.util.Patterns
import java.util.regex.Pattern


data class User (var email:String,val pass : String){

    // validating email id
    val EMAIL_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    fun isEmailValid(): Boolean {
        return EMAIL_PATTERN.matcher(email).matches()
    }


    fun isPasswordLengthGreaterThan5(): Boolean {
        return pass.length > 5
    }
}