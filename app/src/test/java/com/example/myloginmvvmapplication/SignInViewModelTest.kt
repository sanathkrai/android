package com.example.myloginmvvmapplication

import com.example.myloginmvvmapplication.ui.SignInViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SignInViewModelTest() {

    lateinit var signInViewModel: SignInViewModel
    @Before
    fun onSetup() {
        signInViewModel = SignInViewModel()
    }
    @Test
    fun sum() {
        Assert.assertEquals(4, signInViewModel.sum(2,2))
    }
}