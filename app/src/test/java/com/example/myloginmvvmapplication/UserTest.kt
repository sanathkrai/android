package com.example.myloginmvvmapplication

import com.example.myloginmvvmapplication.ui.User
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class UserTest {


    @Test
    fun emailValidatorCorrectEmail(){
        val user = User("name_@email.com","")
        assertTrue(user.isEmailValid())
    }

    @Test
    fun emailValidatorInCorrectEmail(){
        val user = User("name_@email","")
        assertFalse(user.isEmailValid())
    }


    @Test
    fun passwordValidatorCorrectPassword(){
        val user = User("name_@email.com","12345678")
        assertTrue(user.isPasswordLengthGreaterThan5())
    }

}